// ==UserScript==
// @name         Avto Pro User Utils
// @description  Some user utils
// @namespace    avto-pro-masterwind
// @author       masterwind
// @version      24.7.11
// @include      https?://avtopro.ua
// @match        https?://avtopro.ua
// @updateURL    https://utils-masterwind.netlify.app/avto.pro.user.js
// @downloadURL  https://utils-masterwind.netlify.app/avto.pro.user.js
// @supportURL   https://bitbucket.org/masterwind/jira/issues/
// @icon         https://utils-masterwind.netlify.app/img/favicon/avto.pro.favicon.svg
// @grant        none
// @noframes
// ==/UserScript==

/* eslint-disable no-multi-spaces */
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==

// DRAFT



// js-btn-partslist-primary-showmore



class BaconFishText {

	constructor(entries, form) {}

	method() {}
}





// document.querySelectorAll('[data-sub-title="Б/В"]').forEach( item => item.closest('tr').remove() )

seen = {}
counter = 0
document.querySelectorAll('#js-partslist-primary tbody tr').forEach( item => {
// 	if ( counter >= 50 ) return

	let [ expandItem, makerItem, codeItem, descriptionItem, deliveryItem, priceItem ] = item.children

	let maker    = makerItem.textContent.trim()
	let code     = codeItem.textContent.trim()
	let delivery = deliveryItem.textContent.trim()
	let price    = priceItem.textContent.trim()

// 	console.log( `item`, item )
// 	console.log( `makerItem`, maker, /*makerItem*/ )
// 	console.log( `codeItem`, code, /*codeItem*/ )

// 	console.log( `delivery`, delivery.match(/Сьогодні|Завтра|У наявності/) )

// 	if ( !delivery.match(/Сьогодні|Завтра|У наявності/) ) {
// 		console.log( `item`, item )
// 		console.log( `deliveryItem`, delivery, deliveryItem )
// 	}

// 	console.log( `code`, code )

	if ( !seen[ maker + '_' + code ] && delivery.match(/Сьогодні|Завтра|У наявності/) ) {
		console.info( `Leave:`, maker, code, price )
		seen[ maker + '_' + code ] = 1
		return
	}

	item.remove()
	console.log( `Removed:`, maker, code, price )

	counter++
} )

console.log( `Seen:`, seen )
