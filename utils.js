// global utils for all userscripts
!window.utils && (window.utils = {});

utils.zones = {
	0 : '.ucoz.ru',
	1 : '.vo.uz',
	2 : '.at.ua',
	3 : '.p0.ru',
	4 : '.3dn.ru',
	5 : '.my1.ru',
	6 : '.clan.su',
	7 : '.moy.su',
	8 : '.do.am',
	9 : '.pp.net.ua',
	A : '.dmon.com',
	B : '.ovo.bg',
	C : '.uweb.ru',
	D : '.webbuilder3d.com',
	a : '.ucoz.ua',
	b : '.ucoz.kz',
	c : '.ucoz.lv',
	d : '.ucoz.com',
	e : '.ucoz.net',
	f : '.ucoz.org',
	g : '.ucoz.co.uk',
	h : '.ucoz.de',
	i : '.ucoz.es',
	j : '.ucoz.hu',
	k : '.ucoz.ae',
	l : '.usite.pro',
	// l : '.ucoz.fr', // deprecated
	m : '.ucoz.ro',
	n : '.ucoz.pl',
	o : '.narod.ru',
	p : '.ucoz.com.br',
	q : '.narod2.ru',
	r : '.ucoz.site',
	// s : '.ucoz.in', // deprecated
	s : '.ucoz.club',
	t : '.clan.la',
	u : '.me.la',
	// v : '.____',
	// w : '.____',
	// x : '.____',
	// y : '.____',
	z : '.ucozmedia.com',
};

// convert uCoz site to domain letter or vice versa
utils.convert = {
	/**
	 * Get site adress and return username in uCoz
	 * example: betatest-0707.ucoz.ru -> 0betatest-0707
	 * @param   {string}  adress  site adress
	 * @return  {string}          username in uCoz
	 */
	site2user : (adress,
		[match, name, subdomain] = adress.trim().match(/^([^.]+)((?:\..+)?\..+\.\w{2,})$/) || [],
		letter = Object.assign({}, ...Object.entries(utils.zones).map(([k, v] ) => ({[v]:k})))[subdomain],
	) => letter && (letter + name),
	/**
	 * Get username in uCoz and return site adress
	 * example: 0betatest-0707 -> betatest-0707.ucoz.ru
	 * OR
	 * Get letter of system subdomain and return system subdomain
	 * example: 0 -> .ucoz.ru || C -> .uweb.ru,
	 * @param   {string}  string  username in uCoz || letter of system subdomain
	 * @return  {string}          site adress || system subdomain
	 */
	user2site : (data,
		string = data.toString().trim(),
		first = string[0],
		name  = string.slice(1),
	) => (string.length !== 1 ? name : '') + utils.zones[first],
	/**
	 * Convert html string to DOM
	 * @param   {string}  html  html string
	 * @return  {[type]}        dom
	 */
	html2DOM : (html,
		dom = document.implementation.createHTMLDocument('example'),
	) => !dom.documentElement.insertAdjacentHTML('beforeend', html) && dom,
	/**
	 * Convert html string to DOM (alternative)
	 * @param  {string}  text  html string
	 * @return {[type]}        dom
	 */
	domParse : text => (new DOMParser).parseFromString(text, 'text/html'),
};

// detect locations
utils.location = {
	/**
	 * Detect url from location
	 * adapted to ';' or '&' separator for get/post parameters
	 * @param   {string}   page  string/url/params
	 * @return  {boolean}
	 */
	has : (page, alternate = page.replace(';', '&'), ) => utils.location.any([page, alternate]),
	/**
	 * Detect any parts of url from location
	 * @param   {array}    pages  array of strings to compare
	 * @return  {boolean}
	 */
	any : pages => !!pages.filter(page => location.href.includes(page)).length,
};

/**
 * Copy to clipboard
 * @param   {string}  text incoming text for copy to clipboard
 * @return  {null}
 */
utils.copyToClipboard = (text,
	tmp = document.body.insertAdjacentHTML('beforeend', `<input id=tifc value="${text}">`),
) => window.tifc && !tifc.select() && document.execCommand('copy') && !tifc.remove();

// sets style
utils.css = {
	/**
	 * Set style
	 * @param   {string}   css  styles
	 * @param   {string}   id   id for style tag (optional)
	 * @return  {boolean}
	 */
	setStyle : (css, id = 'customCSS', ) => window[id]
		? !!(window[id].innerHTML = css)
		: !document[document.head && 'head' || 'body'].insertAdjacentHTML('beforeend', `<style id=${id}>${css}</style>`),
	/**
	 * Set stylesheet from url
	 * @param   {string}   url  stylesheet url
	 * @return  {boolean}
	 */
	setLink : url => !document[document.head && 'head' || 'body'].insertAdjacentHTML('beforeend', `<link href="${url}" rel="stylesheet">`),
};

// templates
utils.template = {
	/**
	 * Render content from template
	 * @param   {string}  template   template for content
	 * @param   {object}  data       content-data for template
	 * @param   {string}  condition  conditional template
	 *                               How to use: ??key=<h1>{{key}}</h1>??
	 *                               if key exists in data, then template inserted
	 * @return  {string}             processed template
	 */
	render : (template, data = {},
		condition = /\?\?(.+?)\?\?/.test(template)
			&& (template = template.replace(/\?\?(.+?)=(.+?)\?\?/g, (match, name, tpl) => data[name] && tpl || '')),
	) => template.replace(/\{\{(.+?)\}\}/g, (match, group) => data[group] || ''),
};


// EXTEND PROTOTYPE
// Form - insert at carret
HTMLTextAreaElement.prototype.insertAtCaret = function(text ) {
	var start = this.selectionStart;
	var end = this.selectionEnd;
	if (start || start == 0 ) {
		this.value = this.value.substring(0, start) + text + this.value.substring(end, this.value.length);
		this.focus();
		this.selectionStart = start + text.length;
		this.selectionEnd   = start + text.length;
	} else {
		this.value += text;
	}
};


// Set type checker
['Arguments', 'Function', 'String', 'Array', 'Object', 'Number', 'Date', 'RegExp']
	.forEach(name => window['is' + name] = obj => toString.call(obj) === `[object ${name}]`);
