// ==UserScript==
// @name         fileMultiuploader
// @namespace    *
// @version      1.4
// @description  custom file multiuploader
// @author       masterwind
// @match        */panel/?a=fm*
// @match        */tmpls/?a=fm*
// @updateURL    https://utils-masterwind.netlify.app/filemultiuploader.user.js
// @downloadURL  https://utils-masterwind.netlify.app/filemultiuploader.user.js
// @icon         https://utils-masterwind.netlify.app/img/filemultiuploader.icon.png
// @grant        none
// @noframes
// ==/UserScript==

// ==ChangeLog==
// 1.4
// - fixed set/unset watermark option
// 1.3
// - updated script icon
// 1.2
// - added skipping large files
// - expand files count limit to 45
// ==/ChangeLog==

(fileMultiuploader => {
	'use strict'
	new fileMultiuploader(_FM)
})(class FileMultiUploader {
	defaults = { maxFilesCount:45 }

	constructor(originalFileManager) {
		Object.assign(this.defaults, originalFileManager.opts)
		this.donor = originalFileManager.uploadForm
		this.initStyles()
		this.initFilesContainer()
		this.hideDefaultUploadFormElements()
	}

	initFilesContainer() {
		// drop previous container
		this.container?.nextElementSibling.remove()
		this.container?.remove()

		// create new
		self.ufm_uploadform.insertAdjacentHTML('afterend', this.filesContainerTemplate)

		this.container = self.fileUploaderField

		// set events
		self.fileUploaderInput.onchange = this.buildImagesPreview?.bind(this)
		self.subbutUpload.onclick = this.submitMultiuploadForm?.bind(this)
	}

	hideDefaultUploadFormElements() {
		// hide old form elements
		document.querySelectorAll('#ufm_filesdiv, #plus-file, #subbutufm_uploadform, #ufm_uploadstatus').forEach(element => element.classList.add('hidden'))
	}

	buildImagesPreview(event) {
		// build...
		this.fileUploadCheck(event.target, this.defaults.maxFilesCount)

		// save files list
		this.allFiles = Array.from(event.target.files)

		// check watermark state
		this.checkWatermarkState()

		// update preview size
		this.container.classList.remove('medium-preview', 'small-preview', 'big-preview')
		this.container.classList.add( this.allFiles.length > 20 ? 'small-preview' : ( this.allFiles.length > 10 ? 'medium-preview' : 'big-preview' ) )
	}

	checkWatermarkState(files = this.allFiles || []) {
		this.hasImages = !!files.filter(item => item.type.includes('image')).length

		self.ufm_wtmr?.classList[ this.hasImages ? 'remove' : 'add' ]('hidden')
		if (self.ufm_wtmr) self.ufm_wtmr.style.display = ''
	}

	fileUploadCheck(input, countLimit) {
		// drop old warnings
		document.querySelectorAll('#fileUploaderField ~ .warning').forEach(element => element.remove())

		// cut files count to limit
		const dataTransfer = new DataTransfer()
		Array.from(input.files).filter(this.filterBySize.bind(this)).filter((file, index) => index < countLimit).forEach(file => dataTransfer.items.add(file))
		input.files = dataTransfer.files

		// upd container class
		input.parentElement.classList[ input.files.length ? 'add' : 'remove' ]('has-files')

		// set files count
		self.subbutUpload.innerText = `Upload ${ input.files.length } files`

		// prepare previews
		self.fileUploaderPreviews.innerHTML = [ ...input.files ].map(file => {
			file.src = file.type.includes('image')
				? URL.createObjectURL(file)
				: this.unknownFileIcon
			return this.previewTemplate.render(file)
		}).join('')
	}

	filterBySize(file) {
		const isSizeLimit = file.size > this.defaults?.maxFileSize
		if (isSizeLimit) this.showSkippedFilesNotify(file)
		return !isSizeLimit
	}

	showSkippedFilesNotify(file) {
		this.container.insertAdjacentHTML('afterend', this.skippedFileTemplate.render({ name:file.name, size:this.formatFileSize(file.size) }))
	}

	formatFileSize(size) {
		if (size > 1073741824) return (size / 1073741824).toFixed(2) + ' Gb'
		if (size > 1048576)    return (size / 1048576).toFixed(2)    + ' Mb'
		return (size / 1024).toFixed(2) + ' Kb'
	}

	appendFiles(files = this.allFiles || [], form) {
		files.forEach((file, index) => form?.append(`file${ index }`, file, file.name))
	}

	setUploadLoader() { this.container.classList.add('uploading') }

	submitMultiuploadForm() {
		this.setUploadLoader()

		// split files array to chunks
		this.chunks = this.prepareChunks()

		// sending chunks
		Promise.allSettled(
			this.chunks.map( form => fetch(self.uCoz.postParams.url, { method:self.uCoz.postParams.type, body:form }) )
		).then(results => results.forEach((result) => {
			if (result.value.ok) {
				result.value.text()
					.then(data => new window.DOMParser().parseFromString(data, 'text/xml'))
					.then(data => {
						self._uParseXML(data)
						this.initFilesContainer()
				})
			}
		}))
	}

	prepareChunks(files = this.allFiles || [], chunkSize = this.defaults?.maxUploadFile || 10) {
		const chunks = []

		for (let i = 0; i < files.length; i += chunkSize) {
			const form = new FormData()
			const chunk = files.slice(i, i + chunkSize)

			// update form entries
			form.set('ssid', self.uCoz.ssid)
			form.set('a', 'fm')
			form.set('l', 1)
			form.set('f', this.defaults?.fu)
			form.set('wtmr', +this.donor.wtmr?.checked)

			// append files to form data
			this.appendFiles(chunk, form)

			chunks.push(form)
		}

		return chunks
	}

	initStyles() {
		self.document.head.insertAdjacentHTML('beforeend', `<style id=customFilesMultiuploadStyle >${ this.styleContent }</style>`)
	}

	filesContainerTemplate = `
	<label id=fileUploaderField class=file-uploader-field data-tip="Drop (or click and select) files here to upload" full-width flex-center threequarter-gap >
		<input id=fileUploaderInput type=file name=file onchange="uCoz.utils.fileUploadCheck(this, 5)" multiple >
		<article id=fileUploaderPreviews flex-center flex-wrap threequarter-gap ></article>
	</label>
	${ self._uButton('Upload', 'b', { text:'Upload files' }) }`

	previewTemplate = `<figure text-center>
		<img src="{{src}}" alt="{{name}}" onload="uCoz.utils.updateFigcaption(this)" >
		<figcaption>{{name}}</figcaption>
	</figure>`

	skippedFileTemplate = `<p class=warning >File <b>{{name}}</b> skipped by size limit <b>{{size}}</b>!</p>`

	unknownFileIcon = `data:image/svg+xml;base64,PHN2ZyBmaWxsPSIjMzMzIiB3aWR0aD0iMzJweCIgaGVpZ2h0PSIzMnB4IiB2aWV3Qm94PSIwIDAgMTAyNCAxMDI0IiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxwYXRoIGQ9Ik04NTQuNiAyODguN0w2MzkuNCA3My40Yy02LTYtMTQuMi05LjQtMjIuNy05LjRIMTkyYy0xNy43IDAtMzIgMTQuMy0zMiAzMnY4MzJjMCAxNy43IDE0LjMgMzIgMzIgMzJoNjQwYzE3LjcgMCAzMi0xNC4zIDMyLTMyVjMxMS4zYzAtOC41LTMuNC0xNi42LTkuNC0yMi42ek03OTAuMiAzMjZINjAyVjEzNy44TDc5MC4yIDMyNnptMS44IDU2MkgyMzJWMTM2aDMwMnYyMTZhNDIgNDIgMCAwIDAgNDIgNDJoMjE2djQ5NHpNNDAyIDU0OWMwIDUuNCA0LjQgOS41IDkuOCA5LjVoMzIuNGM1LjQgMCA5LjgtNC4yIDkuOC05LjQgMC0yOC4yIDI1LjgtNTEuNiA1OC01MS42czU4IDIzLjQgNTggNTEuNWMwIDI1LjMtMjEgNDcuMi00OS4zIDUwLjktMTkuMyAyLjgtMzQuNSAyMC4zLTM0LjcgNDAuMXYzMmMwIDUuNSA0LjUgMTAgMTAgMTBoMzJjNS41IDAgMTAtNC41IDEwLTEwdi0xMi4yYzAtNiA0LTExLjUgOS43LTEzLjMgNDQuNi0xNC40IDc1LTU0IDc0LjMtOTguOS0uOC01NS41LTQ5LjItMTAwLjgtMTA4LjUtMTAxLjYtNjEuNC0uNy0xMTEuNSA0NS42LTExMS41IDEwM3ptNzggMTk1YTMyIDMyIDAgMSAwIDY0IDAgMzIgMzIgMCAxIDAtNjQgMHoiLz48L3N2Zz4K`

	styleContent = `
	#fileUploaderField, #fileUploaderField * { box-sizing:border-box; transition:all .25s linear; }
	#fileUploaderField { border-width:.125rem; padding:.75rem; gap:.75rem; overflow:hidden; }
	#fileUploaderField input[type=file] { opacity:.25; top:-2rem; bottom:0; height:auto; }
	.file-uploader-field article { align-items:center; }
	.file-uploader-field article figure { position:relative; font-size:0; }
	.file-uploader-field article figure img { width:10rem; height:10rem; }
	.file-uploader-field article figure figcaption { position:absolute; font-size:small; padding:.125rem; text-shadow:0 0 3px #fff; bottom:0; }
	.file-uploader-field ~button { margin:.75rem auto 0; display:block; }
	.file-uploader-field:not(.has-files) ~button { display:none; }
	.file-uploader-field.uploading figure:before { content:''; position:absolute; top:0; right:0; bottom:0; left:0; width:auto; height:auto; background:#0009 url('/.s/img/light_wait.svg') no-repeat center; }
	.file-uploader-field.medium-preview article figure img { width:8rem; height:8rem; }
	.file-uploader-field.small-preview article figure img { width:6rem; height:6rem; }`
});
