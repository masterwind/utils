// ==UserScript==
// @name         kinorium2rezka
// @namespace    kinorium2rezka-masterwind
// @version      25.3.7
// @description  try to take over the world!
// @author       masterwind
// @updateURL    https://utils-masterwind.netlify.app/kinorium2rezka.user.js
// @downloadURL  https://utils-masterwind.netlify.app/kinorium2rezka.user.js
// @include      https://*.kinorium.com/*
// @icon         https://images.kinorium.com/web/favicon/favicon_32.png
// @grant        none
// @noframes
// ==/UserScript==
(function(app) {
	'use strict';
	!!app.target && app.checkHost()
})({
	hosts : [
		// 'http://hdrezka.co',
		// 'http://hdrezka.ink',
		// 'http://hdrezka.today',
		// 'http://hdrezka.tv',
		'rezka-ua.in',
		'hdrezka.ag',
		'hdrezka.me',
		'hdrezka.sh',
		'hdrezka.club',
		'rezka.ag',
	],

	target : document.querySelector('[itemprop="alternativeHeadline"]'),

	checkHost : function() {
		if (!this.hosts.length) return

		let hostImg = document.createElement('img')

		this.currentHost = this.hosts.pop()

		console.warn( `Checking host for accessibility: ${ this.currentHost }` )

		hostImg.onload      = this.setLinkIcons.bind(this)
		hostImg.onerror     = this.checkHost.bind(this)
		hostImg.crossorigin = 'anonymous'
		hostImg.src         = `//${ this.currentHost }/i/share.jpg?v=${ Date.now() }`
	},

	setLinkIcons : function(event) {
		let name = this.target?.textContent.trim()

		if ( !name ) return

		if ( !self.k2rStyle ) document.head.insertAdjacentHTML(
			'beforeend',
			`<style id=k2rStyle >
			.try-to-find { display:inline-block; margin:0; font-size:18px; text-decoration:none; }
			.try-to-find:first-of-type { margin-left:16px; }
			.try-to-find.hd-rezka:before { content:'\\1f4fa'; }
			.try-to-find.ex-fs:before { content:'\\1f3ac'; }
			.try-to-find.ua-kino-club:before { content:'\\1f3a6'; }
			.try-to-find.imcdb:before { content:'\\1f69a'; }
			@media ( width >= 1920px ) {
				:is( .modalBluring > .main-container ) {
					width:1440px; min-width: 1440px;
					:is( .topMenu, .main-table, .serial-rating-table-wrap ) { min-width: unset; max-width: unset; }
				}
			}
			</style>`
		)

		let linksData = [
			{ className:'try-to-find hd-rezka',     title:'HDrezka - Знайти фільм для перегляду',        href:`//${ this.currentHost }/search/?do=search&subaction=search&q={{query}}` },
			{ className:'try-to-find ex-fs',        title:'EX-FS - Знайти фільм для перегляду',          href:`//ex-fs.net/?do=search&subaction=search&story={{query}}` },
			{ className:'try-to-find ua-kino-club', title:'UAKINO.CLUB - Знайти фільм для перегляду',    href:`//uakino.club/index.php?do=search&story={{query}}` },
			{ className:'try-to-find imcdb',        title:'IMCDB - Знайти всі автомобілі даного фільму', href:`//imcdb.org/movies.php?titleMatch=1&title={{query}}` },
		]

		linksData.map( item => {
			let link = document.createElement('a')

			Object.assign(link, item, { href:item.href.replace(/\{\{query\}\}/, encodeURIComponent(name)), target:'_blank' })

			this.target.insertAdjacentElement( 'beforeend', link )
		} )
	},
});
