// ==UserScript==
// @name         Fill Form - Bacon Ipsum
// @description  Right click fill Lorem Ipsum
// @namespace    rc-fill-masterwind
// @author       masterwind
// @version      25.1.7
// @include      *
// @match        *
// @exclude      file://*
// @updateURL    https://utils-masterwind.netlify.app/rcfill.user.js
// @downloadURL  https://utils-masterwind.netlify.app/rcfill.user.js
// @supportURL   https://bitbucket.org/masterwind/jira/issues/
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @run-at       context-menu
// @grant        GM_registerMenuCommand
// @noframes
// ==/UserScript==

/* eslint-disable no-multi-spaces */

function registerFillFocusedFormHandler(event) {
	// FILL FOCUSED FORM
	new BaconFishText(null, document.activeElement?.form)
}

function registerFillAllFormHandler(event) {
	// FILL FORMS
	[ ...document.forms ].forEach(form => { new BaconFishText(null, form) })
}

GM_registerMenuCommand('Fill Focused Form', registerFillFocusedFormHandler)
GM_registerMenuCommand('Fill All Form',     registerFillAllFormHandler)


class BaconFishText {
	storageDataKey = 'baconData'
	storageTaskKey = 'taskIds'
	limit = 1
	form
	edit

	constructor(entries, form) {
		if (entries) this.limit = entries

		if (self._uWnd && !document.addEditForm) self._uWnd.close()
		this.form = form || document.addform || document.addEditForm

		if (!this.form) return

		this.edit = !!this.form.elements?.id
		this.getBacon()
	}

	getBacon() {
		if (this.edit) this.limit = 1

// 		TODO FIXME
// 		fetch('//baconipsum.com/api/?callback=?&sentences=120').then(data => data.json()).then( CALLBACK )

		if (localStorage.getItem(this.storageDataKey)) this.getStorageData()
		else document.defaultView?.$?.getJSON('//baconipsum.com/api/?callback=?', { sentences:120 }, this.saveRawData.bind(this))
	}

	getStorageData(data = []) {
		let storedData = JSON.parse(localStorage.getItem(this.storageDataKey))
		storedData = storedData.pop().split(/\.\s?/).filter(item => !!item)

		data = [ ...Array(storedData.length) ].map(item => storedData[ this.random() ] || storedData[ this.random() ]).filter(item => !!item)

		this.data = [ data.join('. ') ]
		// storedData

		this.prepareData()
		this.fillForm()
	}

	setStorageData(data) {
		localStorage.setItem(this.storageDataKey, JSON.stringify(data))
	}

	saveRawData(data, status, xhr) {
		this.data = data

		this.setStorageData(data)
		this.prepareData()
		this.fillForm()
	}

	fillForm() {
		if (!this.limit) return
		this.limit--

		let contentLengthDefault = 3
		let contentLength = {
			title     : 7,
			mcmessage : 6,
			name      : 5,
			message   : 4,
		};
		let contentType = {
			description : 'sentences',
			descr       : 'sentences',
			brief       : 'sentences',
			message     : 'sentences',
			question    : 'sentences',
		}

		let fieldsList = [ ...this.form.elements ]
			.filter( element => ( element.type == 'text' || element.type == 'textarea' ) && !element.readOnly )
			.filter( element => ( element.name != 'ownurl' ) )

		// do simple text fields
		fieldsList.forEach( element => {
			let value = this.prepareData[ contentType[ element.name ] || 'words' ]( contentLength[ element.name ] || contentLengthDefault )

			if ( element.maxLength > 0 && value.length > element.maxLength ) value = value.substr(0, element.maxLength)

			element.value = value;
			delete this.prepareData.oneSentance
		} )

		// override for numbers
		fieldsList
			.filter( this.#filterFieldAsNumber.bind( this ) )
			.forEach( element => {
				element.value = parseInt(Math.random(10) * 100)
				if ( element.name == 'art' ) element.value = parseInt(Math.random(10) * 10000)
			} )
	}

	#filterFieldAsNumber( element ) {
		if ( element.type == 'number' ) return true
		return this.#fieldsNamesAsNumber().includes( element.name )
	}

	#fieldsNamesAsNumber() {
		return [ 'price', 'price_in', 'price_old', 'weight', 'art', 'stock' ]
	}

	setFieldValue(name, value) {
		this.field = this.form.elements[ name ]

		if (this.field && value) {
			if ( this.field?.maxLength > 0 && value.length > this.field?.maxLength ) value = value.substr(0, this.field?.maxLength)

			this.field.value = value
		}
	}

	updateTask(data) {
		// console.log( 'updateTask::data', data )
		if ( !this.edit ) return
		if ( !data.length ) return

		// TODO
		// ids = JSON.parse(self.sessionStorage.updIds)
		// console.log( 'ids', ids, typeof ids )
		// id = ids.pop()
		// console.log( 'id', id, ids, location )
		// self.sessionStorage.setItem('updIds', JSON.stringify(ids))
		// editLink = `/load/0-0-0-${ +id }-13`
		// console.log( 'editLink', editLink )
		// location.assign(editLink)
	}

	prepareData() {
		let data = this.data.pop().split(/\.\s?/).filter(item => !!item)

		this.prepareData.words = (count = 1) => {
			return (
				this.prepareData.oneSentance
				|| (this.prepareData.oneSentance = data.pop())
			)?.split(' ').slice(0, count).join(' ')
		}

		this.prepareData.sentences = (count = 1) => {
			let content = [ ...Array(count) ].map(() => data.pop()).join('. ')
			let view    = document.defaultView
			let module  = view.uCoz?.module

			if (document.addform && module && module != 'forum') {
				let imgContent = '$IMAGE1$\n' + content + '.\n$IMAGE2$\n$IMAGE3$\n$IMAGE4$\n'
				return imgContent

			} else return content
		}

		this.prepareData.email = (count = 1) => [
			this.prepareData.oneSentance.split(' ').slice(-3)[0],
			this.prepareData.oneSentance.split(' ').slice(-2).join('.'),
		].join('@')
	}

	random(num = 100, rnd = Math.random() * num) {
		rnd = Math.trunc(rnd)
		return (rnd == self.prevRnd) ? this.random() : self.prevRnd = rnd
	}
}
