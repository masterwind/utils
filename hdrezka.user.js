// ==UserScript==
// @name         hdrezka
// @namespace    masterwind
// @version      25.3.9
// @description  some useful player features
// @author       masterwind
// @updateURL    https://utils-masterwind.netlify.app/hdrezka.user.js
// @downloadURL  https://utils-masterwind.netlify.app/hdrezka.user.js
// @include      *//hdrezka.ag/*.html
// @include      *//hdrezka.club/*.html
// @include      *//hdrezka.co/*.html
// @include      *//hdrezka.ink/*.html
// @include      *//hdrezka.me/*.html
// @include      *//hdrezka.sh/*.html
// @include      *//hdrezka.today/*.html
// @include      *//hdrezka.tv/*.html
// @include      *//rezka-ua.in/*.html
// @include      *//rezka.ag/*.html
// @icon         https://static.hdrezka.ac/templates/hdrezka/images/favicon.ico
// @run-at       document-body
// @grant        none
// ==/UserScript==

(function() {
	'use strict';

	// set custom styles
	document.head?.insertAdjacentHTML(
		'beforeend',
		`<style>
		:is( body ) :is( #wrapper ) #main {
			.b-content__main :is( .b-post__description ) ~ div:has( ins ) { display:none; }
			.b-content__columns { padding:1.5rem 0 0; }
			.b-content__columns .b-content__main + div { display:none; }
		}

		@media ( width >= 1920px ) {
			:is( body ) * { box-sizing:border-box; }
			:is( body ) :is( #wrapper, #footer ) { width:auto; margin:auto; }
			:is( body ) :is( #wrapper, #footer ) :is( .b-wrapper ) { max-width:100rem; width:auto; margin:auto; padding:0; }
			:is( body ) :is( #wrapper ) #main {
				#top-nav { padding:0; height:auto; }
				.b-topnav { display:flex; flex-direction:row-reverse;   justify-content:space-between; gap:1rem; }

				form:is( .b-search__form, .b-search__form.focused ) { position:unset; width:25%; display:flex; gap:.5rem; height:auto; flex-direction:row-reverse; }
				form {
					:is( input, button ) { position:unset; height:auto!important;; padding:0 !important; line-height:2.5rem; }
					:is( button ):before { position:unset; margin:0 auto; width:1rem; height:1rem; background-size:1rem; }
					:is( .b-search__live ) { padding:0; padding-top:2.5rem; left:auto; right:0; min-width:25%; width:max-content; }
					:is( .b-search__live ) :is( .b-search__live_section, .b-search__live_all ) { margin:0; }
				}

				#topnav-menu:before { display:none; }
				#topnav-menu { padding:0; display:flex; gap:1.5rem; }
				#topnav-menu >li { margin:0; }
				#topnav-menu >li > a { padding:0; line-height:2.5rem; }
				#topnav-menu >li > .b-topnav__sub { padding-top:2.5rem; }

				--player-height:35rem;
				#cdnplayer-container, #youtubeplayer { width:auto; height:auto; min-width:100%; min-height:var(--player-height); }
				#cdnplayer-container #cdnplayer { width:auto; height:auto; min-width:100%; min-height:var(--player-height)!important; }
				#cdnplayer-preloader { width:100%; min-height:var(--player-height); }
			}
			:is( body ) :is( #footer ) { padding-block:1rem; }
		}
		</style>`
	)
})();
